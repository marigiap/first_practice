package com.amdocs.web.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.amdocs.Calculator;
import com.amdocs.Increment;
import com.amdocs.config.SpringWebConfig;

public class IncrementTest {

    @Test
    public void test1() throws Exception {

        int k= new Increment().decreasecounter(1);
        assertEquals("Add", 1, k);

    }

    @Test
    public void test2() throws Exception {

        int k= new Increment().decreasecounter(0);
        assertEquals("Add", 1, k);

    }
    @Test
    public void test3() throws Exception {

        int k= new Increment().decreasecounter(2);
        assertEquals("Add", 1, k);

    }
}

